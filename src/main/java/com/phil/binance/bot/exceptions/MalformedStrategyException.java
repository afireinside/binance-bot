package com.phil.binance.bot.exceptions;

public class MalformedStrategyException extends Exception {
  public MalformedStrategyException(String message) {
    super(message);
  }
}
