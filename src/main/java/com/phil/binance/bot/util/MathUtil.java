package com.phil.binance.bot.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtil {

  public static BigDecimal divide(BigDecimal numerator, int denominator) {
    return numerator.divide(new BigDecimal(denominator), 2, RoundingMode.HALF_DOWN);
  }

  public static BigDecimal divide(BigDecimal numerator, BigDecimal denominator) {
    return numerator.divide(denominator, 2, RoundingMode.HALF_DOWN);
  }

  public static BigDecimal multiply(String left, String right) {
    return new BigDecimal(left).multiply(new BigDecimal(right));
  }
}
