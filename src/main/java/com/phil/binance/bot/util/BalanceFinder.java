package com.phil.binance.bot.util;

import com.phil.binance.bot.response.acount.Balance;

import java.util.List;

public class BalanceFinder {

  public static Balance find(String assetToFind, List<Balance> balances) {
    Balance balance = null;
    for (Balance curBalance : balances) {
      if (curBalance.getAsset().equals(assetToFind)) {
        balance = curBalance;
        break;
      }
    }

    return balance;
  }
}
