package com.phil.binance.bot.enums;

public class TimeInForce {

  public static final String GOOD_TIL_CANCELLED =
      "GTC"; // An order will be on the book unless the order is canceled.
  public static final String IMMEDIATE_OR_CANCELLED =
      "IOC"; // An order will try to fill the order as much as it can before the order expires.
  public static final String FILL_OR_KILL =
      "FOK"; // An order will expire if the full order cannot be filled upon execution.
}
