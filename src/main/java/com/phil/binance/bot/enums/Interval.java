package com.phil.binance.bot.enums;

import lombok.Getter;

@Getter
public enum Interval {
  MINUTES("m"),
  HOURS("h"),
  DAYS("d"),
  WEEKS("w"),
  MONTHS("M");

  private final String shortName;

  Interval(String shortName) {
    this.shortName = shortName;
  }
}
