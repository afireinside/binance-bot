package com.phil.binance.bot.enums;

import lombok.Getter;

@Getter
public enum Side { // TODO change to class using final static Strings instead
  BUY,
  SELL;
}
