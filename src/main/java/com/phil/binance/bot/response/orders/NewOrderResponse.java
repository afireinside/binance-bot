package com.phil.binance.bot.response.orders;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class NewOrderResponse {

  private String symbol;
  private long orderId;
  private int orderListId;
  private String clientOrderId;
  private long transactTime;
  private String price;
  private String origQty;
  private String executedQty;
  private String cumulativeQuoteQty;
  private String status;
  private String timeInForce;
  private String type;
  private String side;
  private List<OrderFill> fills;
}
