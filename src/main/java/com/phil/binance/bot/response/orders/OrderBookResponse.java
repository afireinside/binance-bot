package com.phil.binance.bot.response.orders;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class OrderBookResponse {

  @JsonProperty("lastUpdateId")
  private long lastUpdateId;

  @JsonProperty("bids")
  private List<List<String>> bids;

  @JsonProperty("asks")
  private List<List<String>> asks;
}
