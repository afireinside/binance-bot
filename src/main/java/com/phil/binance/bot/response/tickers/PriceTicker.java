package com.phil.binance.bot.response.tickers;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PriceTicker {

  private String symbol;
  private String price;
}
