package com.phil.binance.bot.response.acount;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Balance {

  private String asset;
  private String free;
  private String locked;
}
