package com.phil.binance.bot.response.acount;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Account {

  private int makerCommission;
  private int takerCommission;
  private int buyerCommission;
  private int sellerCommission;
  private boolean canTrade;
  private boolean canWithdraw;
  private boolean canDeposit;
  private long updateTime;
  private String accountType;
  private List<Balance> balances;
}
