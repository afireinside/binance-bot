package com.phil.binance.bot.strategies;

import com.phil.binance.bot.client.PrivateClient;
import com.phil.binance.bot.client.PublicClient;
import com.phil.binance.bot.config.EngineConfig;
import com.phil.binance.bot.config.StrategyConfig;
import com.phil.binance.bot.enums.Side;
import com.phil.binance.bot.enums.TimeInForce;
import com.phil.binance.bot.response.acount.Account;
import com.phil.binance.bot.response.acount.Balance;
import com.phil.binance.bot.response.orders.Order;
import com.phil.binance.bot.response.tickers.PriceTicker;
import com.phil.binance.bot.util.BalanceFinder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
@Service
public class DollarCostAveraging extends Strategy {

  private static final int BUY_SIGNAL = 1;
  private static final int SELL_SIGNAL = 0;
  private static final int NO_SIGNAL = -1;

  private final PrivateClient privateClient;
  private final PublicClient publicClient;
  private final EngineConfig engineConfig;
  private final StrategyConfig strategyConfig;

  private TradingPair tradingPair;

  @Autowired
  public DollarCostAveraging(PrivateClient privateClient, PublicClient publicClient, EngineConfig engineConfig,
      StrategyConfig strategyConfig) {
    super(privateClient, engineConfig);
    this.privateClient = privateClient;
    this.publicClient = publicClient;
    this.engineConfig = engineConfig;
    this.strategyConfig = strategyConfig;

    this.tradingPair = new TradingPair(engineConfig.getTradingPair());
  }

  public boolean isEnabled() {
    return strategyConfig.isDollarCostAveraging();
  }

  public void run() {
    int executionNum = 1;
    long startMillis = getStartDateTimeMillis();

    while (true) {
      // Determine cost basis
      List<Order> allOrders = privateClient.getAllOrders(engineConfig.getTradingPair(), startMillis);
      BigDecimal costBasis = calculateCostBasis(allOrders);

      // TODO move this to engine config as "initial cost basis"
      if (costBasis.equals(BigDecimal.ZERO)) {
        costBasis = new BigDecimal("2.4159281");
      }

      // Get current price of trading pair
      BigDecimal currentPrice = getCurrentPrice();

      // Determine target buy & sell prices
      BigDecimal targetBuyPrice = determineBuyPrice(costBasis, currentPrice);
      BigDecimal targetSellPrice = determineSellPrice(costBasis);

      // Get available purchasing power
      Account account = privateClient.getAccountInformation();
      BigDecimal availablePurchasingPower = determinePurchasingPower(account);

      // Get crypto holding
      BigDecimal cryptoHolding = getCryptoBalance(account);

      // Actionable quantity
      BigDecimal quantity = availablePurchasingPower.divide(currentPrice, RoundingMode.HALF_DOWN);

      printSummary(executionNum, currentPrice, costBasis, cryptoHolding, targetSellPrice, targetBuyPrice,
          availablePurchasingPower, quantity);

      // Determine whether we need to buy or sell coin
      int signal = determineBuySellSignal(allOrders, cryptoHolding, availablePurchasingPower);
      if (signal == BUY_SIGNAL) {
        log.info("Placing LIMIT BUY order");
        log.info("Quantity: {} Target Price: {}", quantity, targetBuyPrice);

        placeLimitOrder(Side.BUY, quantity, targetBuyPrice, TimeInForce.GOOD_TIL_CANCELLED);
      } else if (signal == SELL_SIGNAL) {
        log.info("Placing LIMIT SELL order");
        log.info("Quantity: {} Target Price: {}", cryptoHolding, targetSellPrice);

        placeLimitOrder(Side.SELL, cryptoHolding, targetSellPrice, TimeInForce.GOOD_TIL_CANCELLED);
      } else if (signal == NO_SIGNAL) {
        log.info("No signal found. Continue.");
        // NOOP
      }

      executionNum++;
      log.info("Sleeping for {} ms", engineConfig.getSleepIntervalMs());

      try {
        Thread.sleep(engineConfig.getSleepIntervalMs());
      } catch (InterruptedException ie) {
        log.error("InterruptedException caught while sleeping", ie);
      }
    }
  }

  private void printSummary(int executionNum, BigDecimal currentPrice, BigDecimal costBasis, BigDecimal cryptoHolding,
      BigDecimal targetSellPrice, BigDecimal targetBuyPrice, BigDecimal availablePurchasingPower, BigDecimal quantity) {
    log.info("=================== {} {} ===================", engineConfig.getTradingPair(), executionNum);
    log.info("Current {} price: ${}", engineConfig.getTradingPair(), currentPrice);
    log.info("Cost basis ${}", costBasis);
    log.info("Target SELL price ${}", targetSellPrice);
    log.info("Target BUY price ${}", targetBuyPrice);
    log.info("SELLABLE {} quantity: {}", tradingPair.getLeft(), cryptoHolding);
    log.info("BUYABLE {} quantity: {}", tradingPair.getLeft(), quantity);
    log.info("Available purchasing power {} ${}", tradingPair.getRight(), availablePurchasingPower);
    log.info("================================================");
  }

  private BigDecimal getCurrentPrice() {
    PriceTicker priceTicker = publicClient.getPriceTicker(engineConfig.getTradingPair());
    BigDecimal currentPrice = new BigDecimal(priceTicker.getPrice());
    currentPrice = currentPrice.setScale(2, RoundingMode.CEILING);

    return currentPrice;
  }

  private BigDecimal determinePurchasingPower(Account account) {
    Balance purchasingBalance = BalanceFinder.find(tradingPair.getRight(), account.getBalances());
    BigDecimal availablePurchasingPower = new BigDecimal(purchasingBalance.getFree());
    availablePurchasingPower = availablePurchasingPower.setScale(2, RoundingMode.FLOOR);

    return availablePurchasingPower;
  }

  private BigDecimal getCryptoBalance(Account account) {
    List<Balance> balances = account.getBalances();

    Balance foundBalance = null;
    for (Balance curBalance : balances) {
      if (curBalance.getAsset().equals(tradingPair.getLeft())) {
        foundBalance = curBalance;
      }
    }

    if (foundBalance == null) {
      return BigDecimal.ZERO;
    }

    return new BigDecimal(foundBalance.getFree()).setScale(0, RoundingMode.FLOOR);
  }

  private BigDecimal determineBuyPrice(BigDecimal costBasis, BigDecimal currentPrice) {
    BigDecimal buyPrice;
    BigDecimal dipMargin = new BigDecimal(strategyConfig.getDcaDipMargin());

    if (costBasis.compareTo(BigDecimal.ZERO) == 0) {
      buyPrice = currentPrice;
    } else {
      buyPrice = costBasis.subtract(currentPrice.multiply(dipMargin));
    }

    buyPrice = buyPrice.setScale(2, RoundingMode.CEILING);
    return buyPrice;
  }

  private BigDecimal determineSellPrice(BigDecimal costBasis) {
    BigDecimal profitMargin = new BigDecimal(strategyConfig.getDcaProfitMargin());
    BigDecimal sellPrice = costBasis.multiply(profitMargin);
    sellPrice = sellPrice.setScale(2, RoundingMode.FLOOR);

    return sellPrice;
  }

  private int determineBuySellSignal(List<Order> allOrders, BigDecimal cryptoHolding, BigDecimal purchasePower) {
    if (!allOrders.isEmpty()) {
      Order latestOrder = allOrders.get(0);
      if (latestOrder.isWorking()) {
        return NO_SIGNAL;
      }
    }

    if (cryptoHolding.compareTo(new BigDecimal(5)) > 0) {
      return SELL_SIGNAL;
    }

    if (purchasePower.compareTo(new BigDecimal(5)) > 0) {
      return BUY_SIGNAL;
    }

    return NO_SIGNAL;
  }
}
