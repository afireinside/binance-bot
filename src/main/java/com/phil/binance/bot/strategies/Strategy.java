package com.phil.binance.bot.strategies;

import com.phil.binance.bot.client.PrivateClient;
import com.phil.binance.bot.config.EngineConfig;
import com.phil.binance.bot.enums.Side;
import com.phil.binance.bot.enums.Type;
import com.phil.binance.bot.response.orders.NewOrderResponse;
import com.phil.binance.bot.response.orders.Order;
import com.phil.binance.bot.util.MathUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;

@Slf4j
public abstract class Strategy implements Runnable {

  private static final String START_DATETIME = "2022/01/07 23:59:00";
  private static final String BUY = "BUY";
  private static final String SELL = "SELL";
  private static final String FILLED = "FILLED";

  private final PrivateClient privateClient;
  private final EngineConfig engineConfig;

  public Strategy(PrivateClient privateClient, EngineConfig engineConfig) {
    this.privateClient = privateClient;
    this.engineConfig = engineConfig;
  }

  public abstract void run();

  public abstract boolean isEnabled();

  protected void placeMarketOrder(Side buyOrSell, BigDecimal quantity) {
    switch (engineConfig.getEnvironment()) {
      case "dev":
        log.info("Creating new TEST order...");
        privateClient.newTestOrder(engineConfig.getTradingPair(), buyOrSell, Type.MARKET, quantity, null, null);

        break;
      case "prod":
        log.info("Creating new PROD order...");
        NewOrderResponse response = privateClient.newOrder(engineConfig.getTradingPair(), buyOrSell, Type.MARKET,
            quantity, null, null);

        log.info("Binance market order response {}", response);
        break;
      default:
        log.error("Environment not set. Must be either 'dev' or 'prod");
        System.exit(-1);
    }
  }

  protected void placeLimitOrder(Side buyOrSell, BigDecimal quantity, BigDecimal price, String timeInForce) {
    switch (engineConfig.getEnvironment()) {
      case "dev":
        log.info("Creating new TEST order...");
        privateClient.newTestOrder(engineConfig.getTradingPair(), buyOrSell, Type.LIMIT, quantity, price, timeInForce);

        break;
      case "prod":
        log.info("Creating new PROD order...");
        NewOrderResponse response = privateClient.newOrder(engineConfig.getTradingPair(), buyOrSell, Type.LIMIT,
            quantity, price, timeInForce);

        log.info("Binance market order response {}", response);
        break;
      default:
        log.error("Environment not set. Must be either 'dev' or 'prod");
        System.exit(-1);
    }
  }

  protected BigDecimal calculateCostBasis(List<Order> orders) {
    BigDecimal costBasis = BigDecimal.ZERO;
    costBasis = costBasis.setScale(2, RoundingMode.FLOOR);

    if (orders.isEmpty()) {
      return BigDecimal.ZERO;
    }

    // Latest is closet to index 0
    orders.sort(Comparator.comparing((Order o) -> Long.valueOf(o.getTime())));

    int numFilledOrders = 0;
    for (Order order : orders) {
      if (order.getSide().equals(BUY) && order.getStatus().equals(FILLED)) {
        costBasis = costBasis.add(new BigDecimal(order.getPrice()));
        numFilledOrders++;
      } else if (order.getSide().equals(SELL) && order.getStatus().equals(FILLED)) {
        costBasis = BigDecimal.ZERO;
        numFilledOrders = 0;
      }
    }

    if (costBasis.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ZERO;
    }

    return MathUtil.divide(costBasis, numFilledOrders);
  }

  protected long getStartDateTimeMillis() {
    LocalDateTime localDateTime = LocalDateTime.parse(START_DATETIME,
        DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
    return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
  }
}
