package com.phil.binance.bot.strategies;

import lombok.Getter;
import lombok.Setter;

/*
 * Example: ETCUSD
 * Left currency is what we want to buy
 * Right currency is what we buy with
 */
public class TradingPair {

	@Getter
	@Setter
	private String left;

	@Getter
	@Setter
	private String right;

	TradingPair(String left, String right) {
		this.left = left;
		this.right = right;
	}

	TradingPair(String rawTradingPair) {
		this.left = rawTradingPair.substring(0, 3);
		this.right = rawTradingPair.substring(3, 6);
	}
}
