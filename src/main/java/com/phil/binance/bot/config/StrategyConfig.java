package com.phil.binance.bot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class StrategyConfig { // TODO Break class up using @ConditionalOnProperty

  @Value("${strategy.maco.enabled}")
  private boolean maco;

  @Value("${strategy.maco.short-window}")
  private int shortWindow;

  @Value("${strategy.maco.long-window}")
  private int longWindow;

  @Value("${strategy.sma.enabled}")
  private boolean sma;

  @Value("${strategy.sma.window}")
  private int smaWindow;

  @Value("${strategy.sma.interval}")
  private int interval;

  @Value("${strategy.sma.cadence}")
  private String cadence;

  @Value("${strategy.dca.enabled}")
  private boolean dollarCostAveraging;

  @Value("${strategy.dca.dip-margin}")
  private String dcaDipMargin;

  @Value("${strategy.dca.profit-margin}")
  private String dcaProfitMargin;
}
