package com.phil.binance.bot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class DatabaseConfig {

	@Value("${postgres.url}")
	private String url;

	@Value("${postgres.username}")
	private String username;

	@Value("${postgres.password}")
	private String password;
}
