package com.phil.binance.bot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class BinanceConfig {

  @Value("${binance.exchange.key}")
  private String apiKey;

  @Value("${binance.exchange.secret}")
  private String apiSecret;

  @Value("${binance.exchange.url}")
  private String url;

  @Value("${binance.trading.taker-fee}")
  private String takerFee;

  @Value("${binance.trading.maker-fee}")
  private String makerFee;
}
