package com.phil.binance.bot.request;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class OrderRequest {

  private String tradingPair;
  private String side;
  private String type;
  private String timeInForce;
  private BigDecimal quantity;
  private BigDecimal price;
  private int revcWindow;
  private long timestamp;
}
