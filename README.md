## *Phil's Awesome Crypto Bot*

### **Disclaimer**

I cannot be held responsible for any incurred losses that you may experience when running this bot. This is for
entertainment value only and should be treated as such. Only feed the bot whatever you're willing to lose.

### *RUN 👏 AT 👏 YOUR 👏 OWN 👏 RISK*

Having said that, the bot has been running successfully (bug free, no duplicate orders, no losses) for 5 consecutive
days as of this writing (02/20/2021).

Proof:

![](20.png)
![](15.png)

### Why Binance.us?

[Referral Link](https://accounts.binance.us/en/register?ref=35222416)

Binance has one of the cheapest trading fees around. When you're making trades with the bot *AND* using their *BNB*
coin, the fees are a measly **0.075%** per trade. Compare that to some of the other platforms like Gemini which is a
staggering **0.35%**. Trust me, I wrote a [bot for Gemini](https://gitlab.com/afireinside/gemini-bot) initially and
while it worked, it ate through most of my profits.

### Features

- Monitors a pre-defined trading pair on Binance.us every minute for BUY and SELL signals
- Can run multiple bots (IE one for each trading pair) if you wanted to. Recommend starting with a single bot to test
- Signals are determined by a Simple Moving Average (SMA) strategy that scalps for a profit margin that you set
- Fault tolerant. If the bot dies for whatever reason, another one will start up automatically without intervention
- Bot initially looks at most recent order to determine state. Prevents possible duplicate orders.
- Is secure. You're not sharing your API keys/secrets with anyone else.
- is f*cking awesome

### Pre Requisites (OSX)

You should download and install the following in order to use the bot properly.

- MUST have [Binance.us]((https://accounts.binance.us/en/register?ref=35222416)) account.
- MUST create Binance API key/secret locked to your IP address
- MUST install Java 11 `$ brew install openjdk@11`
- MUST install Gradle `$ brew install gradle`
- SHOULD purchase some `BNB` coin for cheapest trading fees.
- SHOULD install [Homebrew](https://brew.sh/)
- SHOULD install [Docker Desktop / Kubernetes](https://www.docker.com/products/docker-desktop)
- SHOULD install kubectl `$ brew install kubectl`
- SHOULD install [IntelliJ Community](https://www.jetbrains.com/idea/download/#section=mac)
- SHOULD have [Docker Hub](https://hub.docker.com) account
- SHOULD have patience

### Environment Setup

1. In Docker Desktop, go to `Preferences` -> `Kuberenetes` and tick the box that says `Enable Kubernetes`. Restart
   Docker Desktop

### Bot Configuration

```yaml
spring:
  profiles:
    active: {dev|prod} # if set to dev, a test order against /api/v2/order/test will be made
                       # if set to prod, a live order against /api/v2/order will be made
binance:
  exchange:
    url: https://api.binance.us # Do not change
  trading:
    taker-fee: 0.1 # Do not change. Binance taker fee assuming NOT using BNB
    maker-fee: 0.1 # Do not change. Binance maker fee assuming NOT using BNB

engine:
  sleep-interval-ms: 61000 # Do not change. How long the bot sleeps in between runs
  trading-volume: 0.3 # currently using all available balance - will ONLY work with a single bot
  trading-pair: ETHUSD # IMPORTANT
  profit-margin: 1.01 # IMPORTANT

strategy: # Only 1 strategy can be enabled at a time
  maco: # Moving average cross over
    enabled: false
    short-window: 2
    long-window: 20
  sma: # Simple moving average
    enabled: true
    interval-minute: 1
    window: 500
```

### Building & Running The Source Code

From the project directory, run the following in your terminal

```shell
$ ./gradlew clean build
```

Now that you have the project built, you can technically run the code as-is to test if it will work as expected.

```shell
$ java -jar build/libs/binance-bot.1.0.0.jar
```

### Kubernetes

We want this bot to run forever. If you're just using the above command to rely on continuous execution, it will fail if
Binance has a single intermittent issue with their API. Kubernetes solves this.

You'll first have to create the API secrets that the bot will use. You'll need to base64 encode the Binance API key and
secret, copy and paste the base64 encoded strings into the `kubernetes/binance-api-secrets.yml` file.

```shell
$  echo -n {API KEY} | base64
$  echo -n {API SECRET} | base64
```

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: binance
type: Opaque
data:
  key: PASTE BASE64 ENCODED API KEY HERE
  secret: PASTE BASE64 ENCODED API SECRET HERE
```

Once you've updated the `binance-api-secrets.yml`, run the following in the `kubernetes` directory to create the secrets
that the bot will use:

```shell
$ kubectl apply binance-api-secrets.yml
```

```shell
$ chmod +x ./build_tag_push.sh
$ ./build_tag_push.sh -u <docker_user> -p <docker_password> -v <version_tag>
```