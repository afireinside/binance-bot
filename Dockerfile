FROM gcr.io/distroless/java@sha256:c94feda039172152495b5cd60a350a03162fce4f8986b560ea555de4d276ce19

COPY build/libs/binance-bot-*.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]