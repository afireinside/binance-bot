#!/bin/bash

DOCKER_USERNAME=""
VERSION=""
DOCKER_PASSWORD=""

while getopts u:v:p: flag
do
    # shellcheck disable=SC2220
    case "${flag}" in
        u) DOCKER_USERNAME=${OPTARG};;
        v) VERSION=${OPTARG};;
        p) DOCKER_PASSWORD=${OPTARG};;
    esac
done

./gradlew clean build
docker build -t $DOCKER_USERNAME/binance-bot:$VERSION -t $DOCKER_USERNAME/binance-bot:latest .

docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD

docker push $DOCKER_USERNAME/binance-bot:$VERSION
docker push $DOCKER_USERNAME/binance-bot:latest
